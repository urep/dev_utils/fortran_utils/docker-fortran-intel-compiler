#!/bin/sh

export CC=gcc
export FC=ifort
export F70=ifort
export F90=ifort
# source the script for Intel Fortran Compiler environment
. /opt/intel/oneapi/setvars.sh

exec "$@"
