FROM gcc:12

LABEL "fr.inrae.os"="Linux"
LABEL "fr.inrae.gcc.version"=12
LABEL "fr.inrae.ifort.version"=2022.1.0.134

# Environment settings
ENV DOWNLOAD_URL=https://registrationcenter-download.intel.com/akdlm/irc_nas/18703/l_fortran-compiler_p_2022.1.0.134_offline.sh
ENV TERM=xterm
ENV SETUP_FILENAME=setup_ifort.sh

# Install curl package in order to download the Intel fortran compiler setup
RUN apt-get -yq install curl

# Install cmake
ARG CMAKE_VERSION=3.23.2
RUN wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh \
      -q -O /tmp/cmake-install.sh \
      && chmod u+x /tmp/cmake-install.sh \
      && mkdir /usr/bin/cmake \
      && /tmp/cmake-install.sh --skip-license --prefix=/usr/bin/cmake \
      && rm /tmp/cmake-install.sh
ENV PATH="/usr/bin/cmake/bin:${PATH}"

# Download and install Intel fortran compiler
RUN curl $DOWNLOAD_URL --output $SETUP_FILENAME
RUN chmod +x $SETUP_FILENAME
RUN ./$SETUP_FILENAME -r yes -a -s --action=install --eula=accept
RUN rm -vf $SETUP_FILENAME

# Uninstall now unecessary package curl
RUN apt-get --purge remove -y curl

# Copy entry point script
COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh
# Copy hello world demo fortran source code
COPY helloworld.f90 /root/helloworld.f90

ENTRYPOINT ["/root/entrypoint.sh"]
# Run sh shell if no other command provided
CMD ["/bin/sh"]
