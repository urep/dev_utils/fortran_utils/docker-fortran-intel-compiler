# Docker image with Intel Fortran Compiler

[![main CI](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/badges/main/pipeline.svg)](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/-/commits/main "main branch CI pipeline status!")
[![Latest Release](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/-/badges/release.svg)](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/-/releases)

A custom docker image which provide an environment for compiling C/C++ and Fortran projects.

For C/C++, gcc is used on Linux version, MSVC on Windows version.

For Fortran, [Intel Fortran Compiler Classic](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#fortran) is used on both versions.

## Get the image

The generated images can be found in the [registry](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/container_registry/492).

Chose:

- the `latest` tag for the most recent one
- a version number for a release version (ex: 1.1)
- a commit SHA for a version from a specific `main` branch commit

Tags for Windows ends with "-windows", other tags are for linux.
> For Windows this is a work in progress, see issue #1.

## Image generation

> Use these docker images:
>
> - [Docker-in-Docker](https://hub.docker.com/_/docker) version 20
> - [gcc](https://hub.docker.com/_/gcc) version 12
> - [microsoft-windows-servercor](https://hub.docker.com/_/microsoft-windows-servercore) version ltsc2019
> WARNING: the **ltsc2022** version exists but this require an excessively recent Windows version to run on. To ensure a great compatibility we stay with ltsc2019 version. (ltsc=long term support)
> WARNING 2: Visual Studio 2022 (version 17) is not compatible with old windows versions so we use version 2019 (version 16).
> WARNING 3: Intel Fortan Compiler seems to have problems with Visual Studio 2022.

### Locally

Build Linux version: `docker build --pull --file Dockerfile-linux --tag <ChoosenTag> <folderPath>`

Build Windows version: `docker build --pull --file Dockerfile-windows --tag <ChoosenTag> <folderPath>`

### Gitlab

This project have a CI which generate the docker image for each main branch commit or tag creation.

The generated images are pulled in [this project Container Registry](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/container_registry).

## Remove policy

Currently no remove policy. The pulled images are never removed from registry automatically.

## Use the image

Run interactive: `docker run -it ChoosenTag`

Run a specific command: `docker run -it ChoosenTag ifort /root/helloworld.f90`

> `/root/helloworld.f90` is a demo fortran source file for test purposes.

## Related links

- [Intel compiler download page](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#fortran)
- [Intel compiler setup command line options](https://www.intel.com/content/www/us/en/develop/documentation/installation-guide-for-intel-oneapi-toolkits-windows/top/installation/install-with-command-line.html#install-with-command-line)
- For installing MSVC BuildTools in the container:
  - [Blog ralated to this](https://jfreeman.dev/blog/2019/07/09/what-i-learned-making-a-docker-container-for-building-c++-on-windows/)
  - [Official Microsoft tutorial](https://docs.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2022)
  - [Where to download BuildTools](https://visualstudio.microsoft.com/fr/downloads/?q=build+tools)
  - [BuildTools setup command line options](https://docs.microsoft.com/en-us/visualstudio/install/use-command-line-parameters-to-install-visual-studio?view=vs-2022)
  - [List of components id in BuildTools setup](https://docs.microsoft.com/en-us/visualstudio/install/workload-and-component-ids?view=vs-2022)
