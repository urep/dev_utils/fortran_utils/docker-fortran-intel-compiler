# Change Log

## [1.1](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/-/releases/1.1) (2022-06-08)

- Add CMake

## [1.0](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler/-/releases/v1.0) (2022-06-02)

- First linux image working
